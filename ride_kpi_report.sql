with region as (
  select sub.unit_id, sub.unit_name as chapter, par.unit_id as region_id, par.unit_name as region
  from w_prsunt_d wpu, w_unt_d sub, w_unt_d par
  where wpu.sub_unit_id = sub.unit_id
  and wpu.parent_unit_id = par.unit_id
  and (wpu.parent_unit_levelno = 6
       or (sub_unit_id = 2107862
           and wpu.parent_unit_levelno = 8
          )
      )
),
all_rides as ( /*all current and previous 2 years*/
  select ev.code,
  case when (substr(ev.code,5,2) in ('MR','VR') or substr(ev.code,7,2) in ('MR','VR'))
  then trim(replace(replace(replace(ev.description,'20'||substr(ev.code,1,2),''),' - ','-'),'  ',' '))
  else ev1.description||
    case when ev.description like '%60%' and ev1.code in (select case when usercode1 is null then substr(code,3,2) else usercode1 end
                                                          from events
                                                          where substr(code,1,2) = substr(extract(year from sysdate),3,2)
                                                          and description like '%60%'
                                                          ) then ' Ride60'
    when ev.description not like '%60%' and ev1.code in (select case when usercode1 is null then substr(code,3,2) else usercode1 end
                                                         from events
                                                         where substr(code,1,2) = substr(extract(year from sysdate),3,2)
                                                         and description like '%60%'
                                                         ) then ' Ride10'
    when ev.code like '%VR%' or (substr(ev.code,3,2) = 'HO'
                                 and (lower(ev.description) like '%virtual%'
                                      or lower(ev.description) like '%vride%'
                                      )
                                 ) then ' vRide'
    else null end end as site,
  wdd.cal_day,
  wdd.cal_year,
  ev.unitid
  from events ev
  left join eventable1 ev1 on (case when ev.usercode1 is null then substr(ev.code,3,2) else ev.usercode1 end=ev1.code)
  left join w_date_d wdd on (to_char(ev.event_start, 'yyyymmdd')=wdd.key)
  left join (select cal_year, max(cal_day) as cal_day
             from w_date_d
             where cal_year in (extract(year from sysdate), extract(year from sysdate)-1, extract(year from sysdate)-2)
             group by cal_year
             ) max_day on (substr(ev.code,1,2)=substr(max_day.cal_year,3,2))
  where substr(ev.code,1,2) in ((substr(extract(year from sysdate),3,2)),(substr(extract(year from sysdate)-1,3,2)),(substr(extract(year from sysdate)-2,3,2)))
  and substr(ev.code,5,2) in ('RI','VR','IR','JD','MR')
  and lower(ev.description) not like '%template%'
  and lower(ev.description) not like '%copy%'
  and lower(ev.description) not like '%test%'
  ),
current_rides as ( /*sites with rides that are taking place in current year*/
  select site, (select cal_day from w_date_d where to_char(sysdate, 'yyyymmdd')=key) - cal_day as days_out, unitid /* -ve #s mean future event */
  from (
    select site, cal_day, unitid,
    row_number() over (partition by site
                       order by cal_day desc nulls last) as rank
    from all_rides
    where substr(code,1,2) = substr(extract(year from sysdate),3,2)
    )
  where rank = 1
  ),
ride_dates as ( /*current and previous 2 years, only sites taking place in current year*/
  select distinct ar.code, ar.site, ar.cal_day, cr.days_out, ar.cal_day + cr.days_out as compare_date, case when ar.cal_day + cr.days_out > 365 then sysdate else wdd.datevalue end as datevalue
  from all_rides ar
  join current_rides cr on (ar.site=cr.site)
  left join w_date_d wdd on (case when ar.cal_year = extract(year from sysdate)
                              or substr(ar.code,1,2) = substr(extract(year from sysdate),3,2)
                              or ar.cal_day + cr.days_out is null then (select cal_day from w_date_d where to_char(sysdate, 'yyyymmdd')=key)
                               when ar.cal_day + cr.days_out < 1 then 1
                               else ar.cal_day + cr.days_out end = wdd.cal_day
                             and case when ar.cal_year is null then substr(ar.code,1,2) + 2000
                               else ar.cal_year end = wdd.cal_year)
  ),
team_donations as (
  select distinct pay.transnum, pay.document_number, pay.payamount, pay.appealcode, rn.groupid
  from payment pay
  left join associated aso on (pay.ltransnum=aso.ltransnum and pay.idnumber=aso.idnumber)
  join registration r on (case when aso.assoc_id is null then pay.idnumber else aso.assoc_id end=r.idnumber and pay.appealcode=r.event)
  join reg_name rn on (rn.regid=r.transnum and rn.name_id=r.idnumber)
  join ride_dates rd on (pay.appealcode=rd.code)
  where (pay.usercode2 = '4020' or pay.usercode2 is null)
  and pay.timestamp <= rd.datevalue
  ),
team_raised as (
  select groupid, appealcode, sum(payamount) as team_raised, count(transnum) as gifts
  from team_donations
  group by groupid, appealcode
  ),
teams as (
  select rd.site, substr(eg.event,1,2) as year, sum(1) as teams, sum(tr.team_raised) as team_raised,
  sum(case when tr.team_raised > 0 then 1 else 0 end) as pledged_team,
  sum(case when tr.team_raised < 750 then 1 else 0 end) as below_min,
  sum(tr.gifts) as gifts
  from event_groups eg
  join ride_dates rd on (eg.event=rd.code)
  left join team_raised tr on (eg.transnum=tr.groupid)
  where eg.transnum in (select rn.groupid
                        from registration r
                        join reg_name rn on (r.transnum=rn.regid and r.idnumber=rn.name_id)
                        where r.usercode5 = '4CN'
                        and (r.classification not in ('SP','IKS')
                             or r.classification is null)
                        )
  and eg.timestamp <= rd.datevalue
  group by rd.site, substr(eg.event,1,2)
  ),
individual_raised as (
  select case when aso.assoc_id is null then pay.idnumber else aso.assoc_id end as fundraiser, rd.site, substr(pay.appealcode,1,2) as year,
  sum(pay.payamount) as ind_raised,
  count(pay.transnum) as gifts
  from payment pay
  join ride_dates rd on (pay.appealcode=rd.code)
  left join associated aso on (pay.ltransnum=aso.ltransnum and pay.idnumber=aso.idnumber)
  where pay.timestamp <= rd.datevalue
  and pay.userflag1 = 'N'
  and (pay.usercode2 = '4020' or pay.usercode2 is null)
  group by case when aso.assoc_id is null then pay.idnumber else aso.assoc_id end, rd.site, substr(pay.appealcode,1,2)
  having sum(pay.payamount) > 0
  ),
participant_list as (
  select rd.site,
  substr(r.event,1,2) as year,
  case when r.timestamp <= rd.datevalue then 1 else 0 end as registered,
  case when ir.ind_raised > 0 then 1 else 0 end as pledged,
  ir.gifts as gifts,
  rd.site||r.idnumber as registration,
  r.classification,
  r.timestamp,
  rd.datevalue
  from registration r
  join ride_dates rd on (r.event=rd.code)
  left join individual_raised ir on (r.idnumber = ir.fundraiser and rd.site = ir.site and substr(r.event,1,2) = ir.year)
  where r.usercode5 = '4CN'
  and (r.classification not in ('SP','IKS')
       or r.classification is null)
  ),
participants as (
  select site, year, sum(registered) as registered, sum(pledged) as pledged, sum(gifts) as gifts
  from participant_list
  group by site, year
  ),
retained_captains as (
  select site,
  sum(case when year = substr(extract(year from sysdate)-1,3,2) and registration in (select registration
                                                                                     from participant_list
                                                                                     where year = substr(extract(year from sysdate)-2,3,2)
                                                                                     and classification like '%TC%'
                                                                                     ) then 1 else 0 end
      ) as previous_retained,
  sum(case when year = substr(extract(year from sysdate),3,2) and registration in (select registration
                                                                                   from participant_list
                                                                                   where year = substr(extract(year from sysdate)-1,3,2)
                                                                                   and classification like '%TC%'
                                                                                   ) then 1 else 0 end
      ) as current_retained
  from participant_list
  where classification like '%TC%'
  and timestamp <= datevalue
  group by site
  ),
donations as (
  select rd.site, substr(pay.appealcode,1,2) as year,
  sum(pay.payamount) as total_donations,
  sum(case when pay.usercode2 = 4020 or pay.usercode2 is null then pay.payamount else null end) as pledged_donations,
  sum(case when pay.usercode2 <> 4020 then pay.payamount else null end) as non_pledged_donations
  from payment pay
  join ride_dates rd on (rd.code=pay.appealcode)
  where pay.timestamp <= rd.datevalue
  group by rd.site, substr(pay.appealcode,1,2)
  )
select re.region,
cr.site,
cr.days_out,
tp.teams as previous_teams,
tc.teams as current_teams,
tc.below_min,
tp.team_raised / tp.teams as previous_avg_team,
tc.team_raised / tc.teams as current_avg_team,
tp.team_raised / tp.pledged_team as previous_avg_plg_team,
tc.team_raised / tc.pledged_team as current_avg_plg_team,
tp.gifts / tp.teams as previous_gft_team,
tc.gifts / tc.teams as current_gft_team,
pp.gifts / pp.pledged as previous_gft_part,
pc.gifts / pc.pledged as current_gft_part,
rc.previous_retained,
rc.current_retained,
dp.pledged_donations as previous_pledged_donations,
dc.pledged_donations as current_pledged_donations,
dp.non_pledged_donations as previous_non_pledged_donations,
dc.non_pledged_donations as current_non_pledged_donations,
dp.total_donations as previous_total_donations,
dc.total_donations as current_total_donations
from current_rides cr
left join region re on (cr.unitid=re.unit_id)
left join teams tp on (cr.site=tp.site and tp.year=substr(extract(year from sysdate)-1,3,2))
left join teams tc on (cr.site=tc.site and tc.year=substr(extract(year from sysdate),3,2))
left join participants pp on (cr.site=pp.site and pp.year=substr(extract(year from sysdate)-1,3,2))
left join participants pc on (cr.site=pc.site and pc.year=substr(extract(year from sysdate),3,2))
left join retained_captains rc on (cr.site=rc.site)
left join donations dp on (cr.site=dp.site and dp.year=substr(extract(year from sysdate)-1,3,2))
left join donations dc on (cr.site=dc.site and dc.year=substr(extract(year from sysdate),3,2))
order by re.region, cr.site