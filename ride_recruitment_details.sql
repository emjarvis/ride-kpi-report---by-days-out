with region as (	
  select sub.unit_id, sub.unit_name as chapter, par.unit_name as region	
  from w_prsunt_d wpu, w_unt_d sub, w_unt_d par	
  where wpu.sub_unit_id = sub.unit_id	
  and wpu.parent_unit_id = par.unit_id	
  and (wpu.parent_unit_levelno = 6	
       or (sub_unit_id = 2107862	
           and wpu.parent_unit_levelno = 8	
           )	
       )	
  ),	
current_sites as (	
  select	
  ev.description as event, ev.code, ev.year, 	
  case when (substr(ev.code,5,2) = 'VR' or substr(ev.code,7,2) = 'VR') 	
    then trim(replace(replace(replace(ev.description,'20'||ev.year,''),' - ','-'),'  ',' '))	
    else ev1.description	
    end as site,	
  row_number() over (partition by (case when (substr(ev.code,5,2) = 'VR' or substr(ev.code,7,2) = 'VR')	
                                   then trim(replace(replace(replace(ev.description,'20'||ev.year,''),' - ','-'),'  ',' '))	
                                   else ev1.description	
                                   end)	
                     order by ev.timestamp desc) as rank,	
  ev.unitid	
  from (	
    select description, code, substr(code,1,2) as year, usercode1, timestamp, unitid	
    from events ev 	
    where substr(code,1,2) in ((substr(extract(year from sysdate),3,2)),(substr(extract(year from sysdate)-1,3,2)),(substr(extract(year from sysdate)-2,3,2)))	
    and substr(code,5,2) in ('RI','VR','MR')	
    and lower(description) not like '%template%'	
    and lower(description) not like '%copy%'	
    and lower(description) not like '%test%'	
    and description not like '%iRide%'	
    ) ev	
  left join eventable1 ev1 on (case when ev.usercode1 is null then substr(ev.code,3,2) else ev.usercode1 end=ev1.code)	
  where ev1.code in (	
    select case when usercode1 is null then substr(code,3,2) else usercode1 end	
    from events	
    where substr(code,1,2) = substr(extract(year from sysdate),3,2)	
    and substr(code,5,2) in ('RI','VR','MR')	
    )	
  ),	
total_raised as (	
  select case when aso.assoc_id is null then pay.idnumber else aso.assoc_id end as idnumber, cs.site, substr(pay.appealcode,1,2) as year, sum(pay.payamount) as donations	
  from payment pay	
  join associated aso on (pay.ltransnum=aso.ltransnum and pay.idnumber=aso.idnumber)	
  left join current_sites cs on (pay.appealcode=cs.code)	
  where (pay.userflag1 <> 'Y' or pay.userflag1 is null)	
  and pay.appealcode in (select code from current_sites)	
  group by case when aso.assoc_id is null then pay.idnumber else aso.assoc_id end, cs.site, substr(pay.appealcode,1,2)	
  ),	
captain as (	
  select rn.groupid, rn.regid as captain_regid, rn.name_id as captain_id,	
  row_number() over (partition by rn.groupid	
                     order by (case when r.update_date is null then r.timestamp else rn.update_date end) desc	
                     )	
  as rank	
  from reg_name rn	
  left join registration r on (rn.regid=r.transnum and rn.name_id=r.idnumber)	
  where r.classification like '%TC%'	
  and r.usercode5 in '4CN'	
  ),	
team_donations as (	
  select groupid, sum(payamount) as team_raised	
  from (	
    select distinct pay.transnum, pay.payamount, rn.groupid	
    from payment pay	
    left join associated aso on (pay.ltransnum=aso.ltransnum)	
    join registration r on (case when aso.assoc_id is null then pay.idnumber else aso.assoc_id end=r.idnumber and pay.appealcode=r.event)	
    join reg_name rn on (rn.regid=r.transnum and rn.name_id=r.idnumber)	
    where rn.groupid is not null	
    )	
  group by groupid	
  ),	
registrations as (	
  select r.idnumber, cs.site, cs.year, cs.code, tr.donations, td.team_raised, cs.event, case when r.idnumber=cap.captain_id then 'Y' else 'N' end as captain,	
  row_number() over (partition by r.idnumber, cs.site	
                     order by cs.year desc	
                     ) as rank	
  from registration r	
  join current_sites cs on (r.event=cs.code)	
  left join reg_name rn on (r.transnum=rn.regid and r.idnumber=rn.name_id)	
  left join total_raised tr on (r.idnumber=tr.idnumber and cs.site=tr.site and cs.year=tr.year)	
  left join captain cap on (rn.groupid=cap.groupid and cap.rank=1)	
  left join team_donations td on (rn.groupid=td.groupid)	
  where cs.year in ((substr(extract(year from sysdate)-1,3,2)),(substr(extract(year from sysdate)-2,3,2)))	
  and r.usercode5 in '4CN'	
  and (r.classification not in ('SP','IKS') or r.classification is null)	
  and (tr.donations >= 1000	
       or r.idnumber=cap.captain_id	
       )	
  ),	
constituents as (	
  select r.idnumber, r.site,	
  case when r.idnumber||r.site in (select idnumber||site	
                                   from registrations	
                                   where captain = 'Y'	
                                   and (donations >= 1000	
                                        or team_raised >= 1000	
                                        )	
                                   ) then 'A'	
  	when r.idnumber||r.site in (select idnumber||site
                                from registrations	
                                where captain = 'N'	
                                and donations >= 1000	
                                ) then 'B'	
    when r.idnumber||r.site in (select idnumber||site	
                                from registrations	
                                where captain = 'Y'	
                                and (donations < 1000 or donations is null)	
                                and (team_raised < 1000 or team_raised is null)	
                                ) then 'C'	
    else null end as segment,	
  case when r.idnumber in (	
    select idnumber	
    from contact	
    where extract(year from actiondate) = extract(year from sysdate)	
    and actioncode = '0PC'	
    and substr(appealcode,1,2) = substr(extract(year from sysdate),3,2)	
    and substr(appealcode,5,2) in ('RI','VR','MR')	
    ) then 1 else 0 end as contacted,	
  case when r.idnumber in (	
    select idnumber	
    from registration	
    where substr(event,1,2) = to_char(sysdate, 'YY')	
    and substr(event,5,2) in ('RI','VR','MR')	
    and usercode5 in ('4CN')	
    ) then 1 else 0 end as registered,	
  case when r.idnumber||r.site in (	
    select r.idnumber||cs.site	
    from registration r	
    join current_sites cs on (r.event=cs.code)	
    where substr(r.event,1,2) = to_char(sysdate, 'YY')	
    and r.usercode5 in ('5DC')	
    ) then 1 else 0 end as declined,	
  r.donations	
  from registrations r	
  where r.idnumber not in (	
    select idnumber	
    from additional_demographics	
    where deceased = 'Y'	
    or dod is not null	
    union	
    select idnumber	
    from interest	
    where intcode in ('NOCO','NC')	
    and (end_date > sysdate or end_date is null)	
    union	
    select idnumber	
    from interest	
    where intcode in ('BRD','EMP','CTEMPL','FMEMP')	
    union	
    select idnumber	
    from prospect	
    where lower(first) like '%anonyme%'	
    or lower(first) like '%anonymous%'	
    or lower(last) like '%anonyme%'	
    or lower(last) like '%anonymous%'	
    or lower(first) like '%unknown%'	
    or lower(last) like '%unknown%'	
    or lower(first) like '%test%'	
    or lower(last) like '%test%'	
    or solicit = 'N'	
    or recordtype = 'O'	
    )	
  and r.rank = 1	
  ),	
email_constituents as (	
  select idnumber	
  from constituents	
  intersect	
  (select idnumber	
   from registration	
   where case when t_date is null then timestamp else t_date end >= sysdate-730	
   and usercode5 = '4CN'	
   union	
   select idnumber	
   from (	
     select idnumber, ltransnum, sum(payamount) as payment_total	
     from payment	
     where case when paydate is null then timestamp else paydate end >=sysdate-730	
     group by idnumber, ltransnum	
     having sum(payamount)>0	
   )	
  )	
  intersect	
  select idnumber	
  from prospect	
  where solicit = 'Y'	
  and emailflag = 'Y'	
  minus	
  select idnumber	
  from interest	
  where intcode in ('NOCO','NC')	
  and (end_date > sysdate or 	
       end_date is null)	
  minus	
  select idnumber	
  from additional_demographics	
  where deceased = 'Y'	
  or dod is not null	
  ),	
address_constituents as (	
  select idnumber	
  from constituents	
  intersect	
  select idnumber	
  from prospect	
  where solicit = 'Y'	
  and mailflag = 'Y'	
  minus	
  select idnumber	
  from interest	
  where intcode in ('NOCO','NC','NODM')	
  and (end_date > sysdate or 	
       end_date is null)	
  minus	
  select idnumber	
  from additional_demographics	
  where deceased = 'Y'	
  or dod is not null	
  )	
select distinct con.idnumber as "CV ID",	
p.first as "First Name",	
p.last as "Last Name",	
re.region as "Region",	
con.site as "Ride Site",	
con.segment as "Segment",	
con.contacted as "Contacted",	
con.registered as "Registered",	
con.declined as "Declined Registration",	
con.donations "Last Ride Raised",	
e.email as "Email",	
case when p.idnumber in (select * from address_constituents) then a.line1 else null end as "Address Line 1",	
case when p.idnumber in (select * from address_constituents) then a.line2 else null end as "Address Line 2",	
case when p.idnumber in (select * from address_constituents) then a.line3 else null end as "Address Line 3",	
a.city as "City",	
a.state as "Province",	
a.zip as "Postal Code"	
from constituents con	
join prospect p on (con.idnumber=p.idnumber)	
left join current_sites cs on (con.site=cs.site)	
left join region re on (cs.unitid=re.unit_id)	
left join email e on (con.idnumber=e.idnumber	
                      and e.preference = 'Y'	
                      and (e.enddate > sysdate or e.enddate is null)	
                      and e.deliverable = 'Y'	
                      and lower(e.email) not like '%offline%'	
                      and lower(e.email) not like '%noemail%'	
                      and lower(e.email) not like '%donordrive%'	
                      and lower(e.email) not like '%jdrf.raisin%'	
                      and lower(e.email) not like '%jdrfraisin%'	
                      and lower(e.email) not like '%r_servation%'	
                      and lower(e.email) not like '%reserve%'	
                      and e.idnumber in (select * from email_constituents)	
                      )	
left join address a on (con.idnumber=a.idnumber	
                        and a.preference = 'Y'	
                        and a.deliverable = 'Y'	
                        and (a.enddate > sysdate or a.enddate is null)	
                       )