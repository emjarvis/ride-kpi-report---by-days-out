# Ride KPI and Recruitment Reports

Ride KPI Report: Year-over-year KPI report for the organization's bike ride events, comparing each ride to the previous year’s ride by the number of days from the current year's event.

Ride Recruitment Analysis Report – Details: Ride participants from the previous two years’ events are divided into 5 ranked segments depending on various criteria. Constituents from segments A-C are listed in this report with contact information and flags indicating if they have been contacted with regards to the current years’ ride, have registered for the current year, or have declined the invitation. Contact information is only printed for those with the applicable communication preferences (and considers CASL compliance).

Ride Recruitment Analysis Report – Summary: Information from the Ride Recruitment Analysis Details report is subtotaled by event site to highlight recruitment KPIs.
